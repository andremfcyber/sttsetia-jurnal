<?php return array (
  'plugins.paymethod.manual.displayName' => 'Pembayaran Biaya Manual',
  'plugins.paymethod.manual.description' => 'Manajer secara manual akan mencatat kwitansi dari pembayaran pengguna (di luar perangkat lunak ini).',
  'plugins.paymethod.manual' => 'Pembayaran Biaya Manual',
  'plugins.paymethod.manual.settings' => 'Pengaturan Pembayaran Manual',
  'plugins.paymethod.manual.settings.instructions' => 'Instruksi',
  'plugins.paymethod.manual.settings.manualInstructions' => 'Instruksi Pembayaran untuk metode pembayaran manual pilihan pengguna',
  'plugins.paymethod.manual.sendNotificationOfPayment' => 'Kirim pemberitahuan dari pembayaran',
  'plugins.paymethod.manual.paymentNotification' => 'Pemberitahuan pembayaran',
  'plugins.paymethod.manual.notificationSent' => 'Pemberitahuan pembayaran terkirim',
  'plugins.paymethod.manual.purchase.title' => 'Judul',
  'plugins.paymethod.manual.purchase.fee' => 'Biaya',
); ?>