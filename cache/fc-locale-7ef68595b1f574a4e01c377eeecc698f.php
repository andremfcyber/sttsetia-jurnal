<?php return array (
  'plugins.importexport.crossref.displayName' => 'Mòdul d\'exportació a XML de CrossRef',
  'plugins.importexport.crossref.description' => 'Exporta les metadades de l\'article en format XML de CrossRef.',
  'plugins.importexport.crossref.cliUsage' => 'Ús:
{$scriptName} {$pluginName} export [xmlFileName] [journal_path] articles objectId1 [objectId2] ...
{$scriptName} {$pluginName} register [journal_path] articles objectId1 [objectId2] ...',
  'plugins.importexport.crossref.requirements' => 'Requisits',
  'plugins.importexport.crossref.requirements.satisfied' => 'Tots els requisits del mòdul han estat satisfets.',
  'plugins.importexport.crossref.settings.form.username' => 'Nom d\'usuari',
  'plugins.importexport.crossref.settings.depositorIntro' => 'Són necessaris els següents elements per a un dipòsit CrossRef amb èxit.',
  'plugins.importexport.crossref.settings.form.depositorName' => 'Nom del dipositant',
  'plugins.importexport.crossref.settings.form.depositorEmail' => 'Correu electrònic del dipositant',
  'plugins.importexport.crossref.settings.form.depositorNameRequired' => 'Introduïu el nom del dipositant.',
  'plugins.importexport.crossref.settings.form.depositorEmailRequired' => 'Introduïu el correu electrònic del dipositant.',
  'plugins.importexport.crossref.senderTask.name' => 'Tasca de registre automàtic de CrossRef',
); ?>