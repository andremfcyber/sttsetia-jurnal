<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<div class="content">
<ul>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/focusandscope" target="_self"><em class="fa fa-search-plus"> Focus and Scope</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/editorialTeam" target="_self"><em class="fa fa-users"> Editorial Team</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/ReviewersAcknowledgement" target="_self"><em class="fa fa-user-md"> Reviewer</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/ReviewersProcess" target="_self"><em class="fa fa-book"> Review Process</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/PublicationEthics" target="_self"><em class="fa fa-list-alt"> Publication Ethics</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/OpenAccesStatement" target="_self"><em class="fa fa-key"> Open Acces Statement</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/PlagiarismScreening" target="_self"><em class="fa fa-check-square-o"> Plagiarism Screening</em></a></li>
<li class="show"><a title="Indexing" href="http://jurnal.sttsetia.ac.id/index.php/pkm/Indexing" target="_self"><em class="fa fa-certificate"> Indexing</em></a></li>
<li class="show"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/copyright_notice" target="_self"><em class="fa fa-copyright"> Copyright Notice<br></em></a></li>
</ul>
<p style="padding: 10px; border-bottom: 1px solid #aaa; background: #00FF00; color: black;"><strong>TEMPLATE</strong></p>
<p><a href="https://drive.google.com/file/d/1o8YDsXGBngfrdIRtgCOq8n2Kq4SeGw8G/view?usp=sharing"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSI9NeBAXHbXEv5dMTEsMy-4kYXMPam-kLr-w&amp;usqp=CAU" alt="Panduan Penulis | Transformasi: Jurnal Pengabdian Masyarakat" width="122" height="39"></a></p>
<p style="padding: 10px; border-bottom: 1px solid #aaa; background: #00FF00; color: black;"><strong>TOOLS</strong></p>
<p><a href="https://www.mendeley.com/?interaction_required=true"><img src="http://sttbi.ac.id/journal/public/site/images/priskilaissak/citation_mendeley.png" width="167" height="33"></a><br><a href="https://www.turnitin.com/"><img src="http://sttbi.ac.id/journal/public/site/images/priskilaissak/turnitin-logo-trans-200.png" width="131" height="36"></a><br><a href="https://www.grammarly.com/"><img src="https://d1zw7v9lpbbx9f.cloudfront.net/static/files/997ea3a3690bda688b2a6d7407bb5eb9/logo.svg" alt="Grammarly" width="128" height="30"></a>&nbsp; &nbsp;</p>
<p><a title="Statcounter" href="https://statcounter.com/p12453594/summary/"><img src="https://statcounter.com/images/button0.png"></a></p>
</div>',
    'id_ID' => '',
  ),
  'context' => 1,
  'enabled' => true,
  'seq' => 2,
); ?>