<?php return array (
  'contactEmail' => 
  array (
    'en_US' => 'jurnal@sttsetia.ac.id',
  ),
  'contactName' => 
  array (
    'en_US' => 'Open Journal Systems',
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'originalFilename' => 'Header Jurnal Setia all.png',
      'uploadName' => 'pageHeaderTitleImage_en_US.png',
      'width' => 3543,
      'height' => 827,
      'dateUploaded' => '2021-02-02 01:34:39',
      'altText' => '',
    ),
  ),
  'showDescription' => true,
  'showThumbnail' => true,
  'showTitle' => true,
  'themePluginPath' => 'default',
); ?>