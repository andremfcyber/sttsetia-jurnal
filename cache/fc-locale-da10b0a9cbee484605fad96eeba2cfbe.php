<?php return array (
  'plugins.generic.tinymce.name' => 'Mòdul d\'integració amb TinyMCE',
  'plugins.generic.tinymce.description' => 'Aquest mòdul activa l\'edició WYSIWYG ("el que veus, és el que tens") a les àrees de text de l\'OMP utilitzant l\'editor de contingut <a href="https://www.tinymce.com" target="_blank">TinyMCE</a>.',
  'plugins.generic.tinymce.settings' => 'Configuració',
); ?>