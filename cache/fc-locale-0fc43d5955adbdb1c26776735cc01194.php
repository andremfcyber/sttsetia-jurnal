<?php return array (
  'plugins.generic.openAIRE.displayName' => 'OpenAIRE',
  'plugins.generic.openAIRE.description' => 'Plugin OpenAIRE menambahkan elemen projectID ke metadata artikel dan memperluas interface OAI-PMH menurut Petunjuk OpenAIRE 1.1, membantu jurnal OJS untuk menjadi OpenAIRE compliant.',
  'plugins.generic.openAIRE.projectID' => 'OpenAIRE ProjectID',
  'plugins.generic.openAIRE.projectIDValid' => 'Masukkan ProjectID yang valid (6 nomor)',
); ?>