<?php return array (
  'agenciesEnabledSubmission' => false,
  'agenciesEnabledWorkflow' => false,
  'agenciesRequired' => false,
  'citationsEnabledSubmission' => true,
  'citationsEnabledWorkflow' => true,
  'citationsRequired' => false,
  'contactEmail' => 'lewibora52@gmail.com',
  'contactName' => 'Lewi Nataniel Bora',
  'contactPhone' => '+6281281100227',
  'copyrightHolderType' => 'context',
  'copyrightNoticeAgree' => false,
  'copyrightYearBasis' => 'issue',
  'copySubmissionAckAddress' => '',
  'copySubmissionAckPrimaryContact' => false,
  'coverageEnabledSubmission' => false,
  'coverageEnabledWorkflow' => false,
  'coverageRequired' => false,
  'defaultReviewMode' => 2,
  'disableUserReg' => false,
  'disciplinesEnabledSubmission' => false,
  'disciplinesEnabledWorkflow' => false,
  'disciplinesRequired' => false,
  'emailSignature' => '<p><br> ________________________________________________________________________</p>
<p>Jurnal PkM Setiadharma</p>',
  'enableAnnouncements' => true,
  'enableAnnouncementsHomepage' => false,
  'enableAuthorSelfArchive' => false,
  'enableClockss' => true,
  'enableLockss' => true,
  'enablePln' => false,
  'enablePortico' => false,
  'envelopeSender' => NULL,
  'itemsPerPage' => 25,
  'keywordsEnabledSubmission' => true,
  'keywordsEnabledWorkflow' => true,
  'keywordsRequired' => false,
  'languagesEnabledSubmission' => false,
  'languagesEnabledWorkflow' => false,
  'languagesRequired' => false,
  'licenseURL' => 'http://creativecommons.org/licenses/by/4.0',
  'mailingAddress' => NULL,
  'membershipFee' => 0,
  'navItems' => NULL,
  'numAnnouncementsHomepage' => 0,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'numPageLinks' => 10,
  'numWeeksPerResponse' => 2,
  'numWeeksPerReview' => 4,
  'onlineIssn' => '2723-7028',
  'printIssn' => '2723-7036',
  'publicationFee' => 0,
  'publisherInstitution' => 'Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta',
  'purchaseArticleFee' => 0,
  'restrictArticleAccess' => false,
  'restrictReviewerFileAccess' => true,
  'restrictSiteAccess' => false,
  'reviewerAccessKeysEnabled' => false,
  'reviewerCompetingInterestsRequired' => true,
  'rightsEnabledSubmission' => false,
  'rightsEnabledWorkflow' => false,
  'rightsRequired' => false,
  'rtAbstract' => true,
  'rtAddComment' => true,
  'rtCaptureCite' => true,
  'rtDefineTerms' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'rtPrinterFriendly' => true,
  'rtSupplementaryFiles' => true,
  'rtViewMetadata' => true,
  'showEnsuringLink' => true,
  'sourceEnabledSubmission' => false,
  'sourceEnabledWorkflow' => false,
  'sourceRequired' => false,
  'subjectsEnabledSubmission' => false,
  'subjectsEnabledWorkflow' => false,
  'subjectsRequired' => false,
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
  ),
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
  ),
  'supportEmail' => NULL,
  'supportName' => NULL,
  'supportPhone' => NULL,
  'themePluginPath' => 'default',
  'typeEnabledSubmission' => false,
  'typeEnabledWorkflow' => false,
  'typeRequired' => false,
  'abbreviation' => 
  array (
    'en_US' => 'JPS',
  ),
  'about' => 
  array (
    'en_US' => '<p><strong>Focus and Scope</strong></p>
<p><strong>Jurnal PkM Setiadharma </strong>is a peer-reviewed journal. Jurnal PkM Setiadharma invites academics and researches who do original searches on community service and Christian Mission. community service focuses on village development and Christian Mission focuses on rural Christian life.</p>
<p><strong>Peer Review Policy</strong></p>
<p>Article that will be published in<strong> Jurnal PkM Setiadharma </strong>carried out through<strong> �blind peer-review� </strong>by considering aspect community services in the village and rural Christian life. Editors and reviewers provide constructive feedback on the evaluation results to the author.</p>
<h3>Review Proces</h3>
<p>Each submitted&nbsp;manuscript&nbsp;will go through&nbsp;a peer-review prosess with the following stages:</p>
<p>1.&nbsp;The editor checks each script�s conditions based on the template provided by the Jurnal PkM Setiadharma.</p>
<p>2.&nbsp;Artikel that have passed the administration will proceed to a blind review.</p>
<p>3.&nbsp;Articles that have gone through a blind review will be returned to the authors with revised or rejected notes.</p>
<p>4.&nbsp;Articles that have been corrected by the author will be reviewed by reviewers. Reviewers will provide publishing recommendations.</p>
<p>5.&nbsp;The editor provides the results of the review to the author by providing information on the revision deadline.</p>
<p>6.&nbsp;The editor will send a ready-to-publish copy to the author before it is issued for approval and cannot be withdrawn.</p>
<p>7.&nbsp;Before articles published, the editor send the article to copyediting section.</p>
<p><strong>Plagiarism Check</strong></p>
<p>The editors will run plagiarism check using Turnitin for each submitted manuscript. If a manuscript has over 20% similarity based on the Turnitin check result, the manuscript will send back to the author to be revised.</p>
<p><strong>Publication Frequency</strong></p>
<p style="text-align: justify; line-height: 18.75pt; background: white; margin: 15.0pt 0cm .0001pt 0cm;"><span style="font-size: 10.5pt; font-family: \'Arial\',\'sans-serif\';">This journal will be issued 3 times yearly: April, August, and December.</span></p>
<p style="text-align: justify; line-height: 18.75pt; background: white; margin: 15.0pt 0cm .0001pt 0cm;"><span style="font-size: 10.5pt;"><strong><span style="font-family: Arial, sans-serif;">Article Processing &amp; Submission Charges</span></strong></span></p>
<p style="text-align: justify; line-height: 18.75pt; background: white; margin: 15.0pt 0cm .0001pt 0cm;"><span style="font-size: 10.5pt;"><span style="font-family: Arial, sans-serif;">All of the process and submission are free of charge.</span></span></p>
<h3 style="line-height: 15.0pt; background: white; margin: 30.0pt 0cm 15.0pt 0cm;"><span style="font-size: 12.0pt; font-family: \'Segoe UI\',\'sans-serif\';">Open Access Policy</span></h3>
<p><span style="font-size: 12.0pt; font-family: \'Segoe UI\',\'sans-serif\';">All of the published articles will be in open access option.</span></p>',
  ),
  'acronym' => 
  array (
    'en_US' => 'JPS',
  ),
  'announcementsIntroduction' => 
  array (
    'en_US' => '<p>Jurnal PkM Setiadharma menerima laporan hasil pengabdian kepada masyarakat <span style="color: rgba(0, 0, 0, 0.87); font-family: \'Source Sans Pro\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; display: inline !important; float: none;">baik dari dosen, mahasiswa, pendeta, aktivis gereja, guru, dan pihak-pihak lain dalam bentuk artikel untuk diterbitkan pada Vol. 2 No (1), April 2021. Artikel merupakan hasil penelitian pengabdian kepada masyarakat dalam lingkup pengembangan desa dan misi Kristen, dan belum pernah diterbitkan sebelumnya. Artikel mengikuti template <a href="https://drive.google.com/file/d/1o8YDsXGBngfrdIRtgCOq8n2Kq4SeGw8G/view"><span style="text-decoration: underline;">Jurnal PkM Setiadharm</span>a</a> dan disubmit di <span style="text-decoration: underline;"><a href="http://jurnal.sttsetia.ac.id/index.php/pkm">OJS PkM Setiadharma</a></span> paling lambat 1 April 2021. B<span style="color: rgba(0, 0, 0, 0.87); font-family: \'Source Sans Pro\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">agi yang belum memiliki akun di jurnal PkM Setiadharma, maka perlu membuat akun terlebih dahulu di&nbsp;</span><a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/submissionshttp://jurnal.sttsetia.ac.id/index.php//user/register">sini</a><span style="color: rgba(0, 0, 0, 0.87); font-family: \'Source Sans Pro\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">. Untuk informasi lebih lanjut, penulis dapat menghubungi editor jurnal ini (Lewi Nataniel Bora), No WA: 081281100227. Tuhan memberkati.</span></span></p>',
  ),
  'authorInformation' => 
  array (
    'en_US' => '<p>Interested in submitting to this journal? We recommend that you review the <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="http://jurnal.sttsetia.ac.id/index.php/index/login">log in</a> and begin the five-step process.</p>',
    'id_ID' => 'Tertarik menerbitkan jurnal? Kami merekomendasikan Anda mereview halaman <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about">Tentang Kami </a>untuk kebijakan bagian jurnal serta <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/submissions#authorGuidelines">Petunjuk Penulis </a>. Penulis perlu <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/user/register">Mendaftar </a>dengan jurnal sebelum menyerahkan atau jika sudah terdaftar <a href="http://jurnal.sttsetia.ac.id/index.php/index/login">login</a>dan mulai proses lima langkah.',
  ),
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => '<p>This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.</p>',
    'id_ID' => 'Jurnal ini memberi izin dan mendorong penulis untuk memposting item yang diserahkan ke jurnal di website personal atau repositori institusional sebelum dan sesudah penerbitan, sementara itu menyediakan detail bibliografi yang akan memberikan kredit, jika bisa diterapkan, penerbitannya di jurnal ini.',
  ),
  'clockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://clockss.org/">More...</a>',
    'id_ID' => 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://clockss.org/">More...</a>',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta',
  ),
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections.

The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.


<h4>Copyediting Systems</h4>

<strong>1. Microsoft Word\'s Track Changes</strong>

Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author.

The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets.

After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage.


<strong>2. Harvard Educational Review</strong>

<strong>Instructions for Making Electronic Revisions to the Manuscript</strong>

Please follow the following protocol for making electronic revisions to your manuscript:

<strong>Responding to suggested changes.</strong>
&nbsp; For each of the suggested changes that you accept, unbold the text.
&nbsp; For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it.

<strong>Making additions and deletions.</strong>
&nbsp; Indicate additions by <strong>bolding</strong> the new text.
&nbsp; Replace deleted sections with: <strong>[deleted text]</strong>.
&nbsp; If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>.

<strong>Responding to Queries to the Author (QAs).</strong>
&nbsp; Keep all QAs intact and bolded within the text. Do not delete them.
&nbsp; To reply to a QA, add a comment after it. Comments should be delimited using:
<strong>[Comment:]</strong>
&nbsp; e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>.

<strong>Making comments.</strong>
&nbsp; Use comments to explain organizational changes or major revisions
&nbsp; e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>
&nbsp; Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.

<h4>An Illustration of an Electronic Revision</h4>

<ol>
<li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li>
<li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li>
<li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li>
</ol>',
    'id_ID' => 'Tahap Copyediting dimaksudkan untuk meningkatkan aliran, kejelasan, tata bahasa, kata-kata, dan pemformatan artikel. Tahap ini mewakili kesempatan terakhir untuk penulis untu membuat perubahan substansial apapun pada teks karena tahap selanjutnya terlarang untuk koreksi format dan kesalahan penulisan.

File untuk dicopyedit berupa file Word atau .rtf sehingga dapat diedit dengan mudah.  Petunjuk yang ditampilkan berikut menggambarkan dua pendekatan dalam melakukan copyediting.  Pendekatan pertama, berdasarkan fitur Track Changes di Microsoft Word yang membutuhkan copy editor, editor dan penulis memiliki program ini. Pendekatan kedua, tidak tergantung piranti lunak tertentu, dipinjam dengan seijin dari Harvard Educational Review. Editor Jurnal dapat memodifikasi petunjuk berikut ini dalam rangka meningkatkan proses editorial di jurnal ini.


<h4>Sistem Copyedit</h4>

<strong>1. Track Changes dari Microsoft Office Word</strong>

Di bawah menu Review di Microsoft Office Word, fitur Track Changes memungkinkan copy editor untuk membuat sisipan (teks diberi warna) dan penghapusan (teks diberi coretan dengan warna atau di margin dan ditandai dengan kata dihapus). Copy editor dapat meninggalkan pertanyaan pada penulis (Pertanyaan ke Penulis) dan pada editor (Pertanyaan ke Editor) dengan menyisipkan pertanyaan-pertanyaan di dalam tanda kurung persegi (tanda [...]). Versi yang sudah dicopyedit kemudian diunggah, dan editor diberitahu. Editor kemudian mereview teks dan memberitahu penulis.

Editor dan penulis cukup membiarkan perubahan yang dibuat copy editor jika telah puas dengan perubahan tersebut. Jika perlu perubahan lebih lanjut, editor dan penulis dapat melakukan perubahan dengan penyisipan dan penghapusan dalam naskah yang telah dicopyedit tersebut. Penulis dan editor harus menjawab setiap pertanyaan dari copy editor dengan mengetikkan responnya di dalam tanda kurung persegi tersebut.

Setelah naskah direview oleh editor dan penulis, copy editor akan melakukan pengecekan terakhir untuk tahap layout dan galley.


<strong>2. Harvard Educational Review</strong>

<strong>Instruksi untuk Membuat Revisi Elektronik dalam Naskah</strong>

Mohon ikuti protokol berikut dalam membuat revisi elekronik untuk naskah Anda:

<strong>Merespon perubahan yang disarankan.</strong>
&nbsp; Untuk masing-masing perubahan yang Anda setujui, ubah teks yang ditebalkan ke huruf biasa.
&nbsp; Untuk masing-masing perubahan yang Anda tidak setuju, masukkan kembali teks asli dan <strong>tebalkan</strong>.

<strong>Menambah dan menghapus.</strong>
&nbsp; Tandai tambahan dengan <strong>menebalkan</strong>teks baru.
&nbsp; Gantikan bagian yang dihapus dengan: <strong>[teks dihapus]</strong>.
&nbsp; Jika Anda menghapus satu kalimat atau lebih, mohon berikan catatan, contoh <strong>[dua kalimat dihapus]</strong>.

<strong>Merespon Pertanyaan untuk Penulis (QA/Queries to the Author).</strong>
&nbsp; Jaga semua QA tetap utuh dan tercetak tebal.  Jangan dihapus.
&nbsp; Untuk menjawab QA, tambahkan komentar setelah pertanyaan tersebut. Komentar harus dibatasi dengan menggunakan:
<strong>[Komentar:]</strong>
&nbsp; contoh: <strong>[Komentar: memperdalam pembahasan metodologi seperti yang Anda sarankan]</strong>.

<strong>Membuat Komentar.</strong>
&nbsp; Gunakan komentar untuk menjelaskan perubahan susunan atau revisi besar
&nbsp; contoh: <strong>[Komentar: Memindahkan paragraf di atas dari hal. 5 ke hal. 7].</strong>
&nbsp; Catatan: Saat merujuk suatu nomor halaman, gunakan nomor halaman dari print out naskah yang telah dikirimkan ke Anda. Hal ini penting karena nomor halaman bisa berubah saat dokumen direvisi secara elektronik.

<h4>Ilustrasi Revisi Elektronik </h4>

<ol>
<li><strong>Copyedit Awal.</strong>  Copy editor jurnal akan mengedit teks untuk meningkatkan alur, kejelasan, tata bahasa, diksi dan format, juga termasuk pertanyaan penulis jika perlu. Saat Copyedit Awal selesai, copy editor akan mengunggah dokumen yang sudah direvisi melalui website jurnal dan memberitahu penulis bahwa ada naskah yang telah diedit dan perlu direview kembali oleh penulis.</li>
<li><strong>Copyedit Penulis.</strong>  Sebelum mengubah drastis struktur dan susunan naskah yang sudah diedit, penulis harus bekerjasama dengan editor yang menangani naskah. Penulis harus menerima atau menolak perubahan apapun yang dibuat selama Copyediting Awal, dan merespon semua Pertanyaan ke Penulis (QA). Saat selesai melakukan revisi, penulis harus mengganti nama file dari NamaPenulisQA.doc menjadi NamaPenulisQAR.doc (contoh: dari LeeQA.doc ke LeeQAR.doc) dan mengunggah dokumen yang telah direvisi melalui website jurnal.</li>
<li><strong>Copyedit Akhir.</strong>  Copy editor jurnal akan memverifikasi perubahan yang dibuat oleh penulis dan menggabungkan respon pada Pertanyaan ke Penulis untuk membuat naskah final. Saat selesai, copy editor akan mengunggah naskah final melalui website jurnal dan memberitahu editor layout untuk melanjutkan ke finalisasi format.</li>
</ol>',
  ),
  'copyrightNotice' => 
  array (
    'en_US' => '<p>Authors who publish with this journal agree to the following terms:</p>
<ol type="a">
<li class="show">Authors retain copyright and grant the journal right of first publication with the work simultaneously licensed under a <a href="http://creativecommons.org/licenses/by/3.0/" target="_new">Creative Commons Attribution License</a> that allows others to share the work with an acknowledgement of the work\'s authorship and initial publication in this journal.</li>
<li class="show">Authors are able to enter into separate, additional contractual arrangements for the non-exclusive distribution of the journal\'s published version of the work (e.g., post it to an institutional repository or publish it in a book), with an acknowledgement of its initial publication in this journal.</li>
<li class="show">Authors are permitted and encouraged to post their work online (e.g., in institutional repositories or on their website) prior to and during the submission process, as it can lead to productive exchanges, as well as earlier and greater citation of published work (See <a href="http://opcit.eprints.org/oacitation-biblio.html" target="_new">The Effect of Open Access</a>).</li>
</ol>',
  ),
  'customHeaders' => 
  array (
    'en_US' => '<meta name="google-site-verification" content="WJHuOa4IpvNwUIktOYJn7cV8UEWy_f9AdhX4kFHwQsY" />',
  ),
  'description' => 
  array (
    'en_US' => '<p>Jurnal PkM Setiadharma menghadirkan tema mengenai Pengabdian kepada Masyarakat. Tema bisa didapatkan dari berbagai bentuk pelayanan masyarakat secara khusus pengembangan pedesaan. Selain itu, Jurnal PkM Setiadharma juga menerima tema mengenai misi penginjilan di pedesaan.</p>',
  ),
  'editorialTeam' => 
  array (
    'en_US' => '<p>Editor in Chief<br>Tony Salurante, M.Th.</p>
<p>Editoral&nbsp; Board<br>Lewi Nataniel Bora</p>',
  ),
  'favicon' => 
  array (
    'en_US' => 
    array (
      'name' => 'Logo SETIA warna.png',
      'uploadName' => 'favicon_en_US.png',
      'width' => 1220,
      'height' => 1220,
      'dateUploaded' => '2020-07-09 08:43:52',
      'altText' => '',
    ),
  ),
  'journalThumbnail' => 
  array (
    'en_US' => 
    array (
      'name' => 'Cover Jurnal PkM.png',
      'uploadName' => 'journalThumbnail_en_US.png',
      'width' => 2480,
      'height' => 3508,
      'dateUploaded' => '2020-07-09 08:59:46',
      'altText' => '',
    ),
  ),
  'librarianInformation' => 
  array (
    'en_US' => '<p>We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).</p>',
    'id_ID' => 'Kami mendorong pustakawan riset untuk mendaftar jurnal diantara pemegang jurnal eletronik perpustakaan. Begitu juga, ini mungkin berharga bahwa sistem penerbitan sumber terbuka jurnal cocok untuk perpustakaan untuk menjadi tuan rumah untuk anggota fakultas untuk menggunakan jurnal saat mereka terlibat dalam proses editing. (Lihat <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://www.lockss.org/">More...</a>',
    'id_ID' => 'OJS sistem LOCKSS berfungsi sebagai sistem pengarsipan terdistribusi antar-perpustakaan yang menggunakan sistem ini dengan tujuan membuat arsip permanen (untuk preservasi dan restorasi). <a href="http://www.lockss.org/">Lanjut...</a>',
  ),
  'metaCitations' => 
  array (
    'en_US' => '1',
    'id_ID' => '1',
  ),
  'name' => 
  array (
    'en_US' => 'Jurnal PKM Setiadharma',
  ),
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.',
    'id_ID' => 'Jurnal ini menyediakan akses terbuka yang pada prinsipnya membuat riset tersedia secara gratis untuk publik dan akan mensupport pertukaran pengetahuan global terbesar.',
  ),
  'pageFooter' => 
  array (
    'en_US' => '<div class="footer-widget">
<div class="fwidget et_pb_widget widget_media_image">&nbsp;<img class="image wp-image-2371  attachment-medium size-medium" style="max-width: 100%; height: auto;" src="https://www.sttsetia.ac.id/wp-content/uploads/2019/09/Logo-header-300x74.png" sizes="(max-width: 300px) 100vw, 300px" srcset="https://www.sttsetia.ac.id/wp-content/uploads/2019/09/Logo-header-300x74.png 300w, https://www.sttsetia.ac.id/wp-content/uploads/2019/09/Logo-header.png 600w" alt="" width="300" height="74"></div>
<br>
<div class="fwidget et_pb_widget widget_media_image">&nbsp;</div>
<div id="media_image-3" class="fwidget et_pb_widget widget_media_image"><strong>Address</strong></div>
</div>
<div class="footer-widget">
<div id="custom_html-5" class="widget_text fwidget et_pb_widget widget_custom_html">
<div class="textwidget custom-html-widget">Jln. Daan Mogot Km 18 Kel. Kebon besar Kec. Batu Ceper (145,63 km) Kota Tangerang 15122</div>
<div class="textwidget custom-html-widget">&nbsp;</div>
<div class="textwidget custom-html-widget">Email</div>
<div class="textwidget custom-html-widget">jurnalsetiadharma@sttsetia.ac.id</div>
<div class="textwidget custom-html-widget">&nbsp;</div>
<div class="textwidget custom-html-widget">Phone</div>
<div class="textwidget custom-html-widget">+6281387772030</div>
<div class="textwidget custom-html-widget">+6281281100227</div>
</div>
</div>',
  ),
  'pageHeaderLogoImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'hEADER sd.png',
      'uploadName' => 'pageHeaderLogoImage_en_US.png',
      'width' => 3750,
      'height' => 781,
      'dateUploaded' => '2020-07-13 05:49:23',
      'altText' => '',
    ),
  ),
  'privacyStatement' => 
  array (
    'en_US' => '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>',
    'id_ID' => '<p>Nama dan alamat email yang dimasukkan di website ini hanya akan digunakan untuk tujuan yang sudah disebutkan, tidak akan disalahgunakan untuk tujuan lain atau untuk disebarluaskan ke pihak lain.</p>',
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p>
	<h4>For Spelling and Grammar Errors</h4>

	<p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p>

	<pre>1. CHANGE...
	then the others
	TO...
	than the others</pre>
	<br />
	<pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre>
	<br />

	<h4>For Formatting Errors</h4>

	<p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p>
	<br />
	<pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre>
	<br />
	<pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
    'id_ID' => '<p>Tahap proofreading dimaksudkan untuk menemukan kesalahan di format, tata bahasa, dan ejaan galley. Perubahan substansi tidak bisa dibuat di tahap ini, kecuali didiskusikan dengan Editor Bagian. Di Layout, klik di VIEW PROOF untuk melihat HTML, PDF, dan format file lain yang tersedia dalam menerbitkan naskah ini.</p>
	<h4>Untuk kesalahan tata bahasa dan ejaan</h4>

	<p>Salin kata atau frase yang menjadi masalah dan letakkan mereka di kotak Koreksi Proofreading dengan instruksi "UBAH_KE" untuk editor seperti contoh berikut ini:</p>

	<pre>1. UBAH...
	di tulis lengkap
	KE...
	ditulis lengkap</pre>
	<br />
	<pre>2. UBAH...
	Malinowsky 
	KE....
	Malinowski</pre>
	<br />

	<h4>Untuk kesalahan format </h4>
	
	<p>Jelaskan letak kesalahan dalam kotak Koreksi Proofreading setelah mengetik judul "FORMAT" seperti contoh berikut ini:</p>
	<br />
	<pre>3. FORMAT
	Nomor dalam Tabel 3 tidak sejajar di kolom ketiga.</pre>
	<br />
	<pre>4. FORMAT
	Paragraf yang dimulai dengan "Topik terakhir ini..." tidak menjorok.</pre>',
  ),
  'readerInformation' => 
  array (
    'en_US' => '<p>We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.</p>',
    'id_ID' => 'Kami mendorong pembaca untuk mendaftarkan diri di layanan notifikasi penerbitan untuk jurnal ini. Gunakan tautan <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/user/register">Daftar</a>di bagian atas beranda jurnal. Dengan mendaftar, pembaca akan memperoleh email berisi Daftar Isi tiap ada terbitan jurnal baru. Daftar ini juga membuat jurnal dapat mengetahui tingkat dukungan atau jumlah pembaca. Lihat jurnal <a href="http://jurnal.sttsetia.ac.id/index.php/pkm/about/submissions#privacyStatement">Pernyataan Privasi</a>, yang meyakinkan pembaca bahwa nama dan alamat email yang didaftarkan tidak akan digunakan untuk tujuan lain.',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4>
	<p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p>
	<h4>A. When the Author Provides a Link with the Reference</h4>
	<ol>
	<li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>B. Enabling Readers to Search Google Scholar For References</h4>
	<ol>
		<li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title�e.g., "Peace"�then copy author and title).</li>
		<li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&hl=en&lr=&btnG=Search.</li>

	<li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>C. Enabling Readers to Search for References with a DOI</h4>
	<ol>
	<li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li>
	<li>Paste each DOI that the Query provides in the following URL (between = and &): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&link_type=DOI.</li>
	<li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li>
	</ol>',
    'id_ID' => '<h4>Menambah Tautan Referensi ke Proses Layout </h4>
	<p>Saat mengubah naskah menjadi HTML atau PDF, pastikan semua hyperlink dalam naskah aktif.</p>
	<h4>A. Jika Penulis Memberikan Tautan bersama dengan Referensi</h4>
	<ol>
	<li>Saat naskah masih dalam format pengolah kata (contoh: MS Office Word), tambahkan frase LIHAT ITEM di tiap akhir referensi yang mempunyai URL.</li>
	<li>Ubah frase ke hyperlink dengan meng-highlightnya dan menggunakan alat Insert Hyperlink di Word, masukkan URL yang dipersiapkan di #2.</li>
	</ol>
	<h4>B. Memberi Pembaca Akses untuk Mencari Referensi dengan Google Scholar</h4>
	<ol>
	<li>Saat naskah masih dalam format pengolah kata (contoh: MS Office Word), salin judul referensi di Daftar Pustaka (jika judulnya tampak terlalu umum, contoh: "Damai"-salin nama penulis dan judul).</li>
	<li>Tempel judul referensi antara %22, tambahkan tanda "+" di antara kata-kata: http://scholar.google.com/scholar?q=%22TULIS+JUDUL+DI+SINI%22&hl=en&lr=&btnG=Search.</li>

	<li>Tambahkan frase GS SEARCH di tiap akhir referensi di daftar pustaka naskah.</li>
	<li>Ubah frase itu ke hyperlink dengan meng-highlightnya dan menggunakan alat Insert Hyperlink di Word, masukkan URL yang dipersiapkan di #2.</li>
	</ol>
	<h4>C. Memberi Pembaca Akses untuk Mencari Referensi dengan DOI</h4>
	<ol>
	<li>Saat naskah masih dalam format pengolah kata (contoh: MS Office Word), salin sejumlah referensi ke dalam CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li>
	<li>Tempel tiap DOI yang dihasilkan di URL berikut ini (di antara = and &): http://www.cmaj.ca/cgi/external_ref?access_num=TULIS_DOI_DISINI&link_type=DOI.</li>
	<li>Tambahkan frase CrossRef di akhir tiap sitasi di daftar pustaka naskah.</li>
	<li>Ubah frase menjadi hyperlink dengan memilih frase dan menggunakan alat Insert Hyperlink di Word, masukkan URL yang telah dipersiapkan di #2.</li>
	</ol>',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<p>1. Berikan komentar pada judul yang diberikan oleh penulis<br>2. berikan komentar pada metode penelitian dan PkM yang digunakan<br>3. Berikan komentar pada pendahuluan<br>4. Berikan komentar pada hasil penelitian dan PkM<br>5. Berikan Komentar pada Kesimpulan dengan memperhatikan metode dan pembahasannya<br>6. Berikan Komentar pada cara pengutipan<br>7. Berikan Komentar pada abstrak dengan memperhatikan judul, metode, pembahasan, dan kesimpulan yang diambil oleh penulis</p>',
  ),
  'searchDescription' => 
  array (
    'en_US' => 'Jurnal PkM Setiadharma',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'content' => 'The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).',
        'order' => '1',
      ),
      1 => 
      array (
        'content' => 'The submission file is in OpenOffice, Microsoft Word, or RTF document file format.',
        'order' => '2',
      ),
      2 => 
      array (
        'content' => 'Where available, URLs for the references have been provided.',
        'order' => '3',
      ),
      3 => 
      array (
        'content' => 'The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.',
        'order' => '4',
      ),
      4 => 
      array (
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.',
        'order' => '5',
      ),
    ),
    'id_ID' => 
    array (
      0 => 
      array (
        'content' => 'Naskah belum pernah diterbitkan sebelumnya, dan tidak sedang dalam pertimbangan untuk diterbitkan di jurnal lain (atau sudah dijelaskan dalam Komentar kepada Editor).',
        'order' => '1',
      ),
      1 => 
      array (
        'content' => 'File naskah dalam format dokumen OpenOffice, Microsoft Word, atau RTF.',
        'order' => '2',
      ),
      2 => 
      array (
        'content' => 'Referensi yang dapat diakses online telah dituliskan URL-nya.',
        'order' => '3',
      ),
      3 => 
      array (
        'content' => 'Naskah diketik dengan teks 1 spasi; font 12; menggunakan huruf miring, bukan huruf bergaris bawah (kecuali alamat URL); dan semua ilustrasi, gambar, dan tabel diletakkan dalam teks pada tempat yang diharapkan, bukan dikelompokkan tersendiri di akhir naskah.',
        'order' => '4',
      ),
      4 => 
      array (
        'content' => 'Pengetikan naskah dan sitasi mengikuti gaya selingkung yang disyaratkan dalam Panduan Penulis',
        'order' => '5',
      ),
    ),
  ),
); ?>