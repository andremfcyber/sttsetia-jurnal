<?php return array (
  'doiIssueSuffixPattern' => NULL,
  'doiPrefix' => '10.47457',
  'doiRepresentationSuffixPattern' => NULL,
  'doiSubmissionSuffixPattern' => NULL,
  'doiSuffix' => 'default',
  'enabled' => true,
  'enableIssueDoi' => true,
  'enableRepresentationDoi' => true,
  'enableSubmissionDoi' => true,
); ?>