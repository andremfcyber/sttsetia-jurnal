<?php return array (
  'plugins.metadata.mods34.displayName' => 'Metadata MODS 3.4',
  'plugins.metadata.mods34.description' => 'Berkontribusi terhadap skema dan adapter aplikasi MODS 3.4.',
  'plugins.metadata.mods34.mods34XmlOutput.displayName' => 'Output MODS 3.4',
  'plugins.metadata.mods34.mods34XmlOutput.description' => 'Mengkonversi deskripsi metadata MODS 3.4 menjadi bundle XML.',
); ?>