<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<div id="customblock-sidebar" class="pkp_block block_custom">
<div class="content">
<div id="customblock-Information" class="pkp_block block_custom">
<div class="content">
<div style="background-color: #8508a5; color: white; padding: 10px; border: 1px solid #DD251D; border-radius: 5px; width: 200px;">
<p style="text-align: center;"><strong>INFORMATION</strong></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/focus_and_scope"><strong>FOCUS AND SCOPE</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/publication_ethics"><strong>ETHICS STATEMENT</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/authorguide"><strong>AUTHOR GUIDELINES</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/AuthorFees"><strong>AUTHOR FEES</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/peerreviewprocess"><strong>PEER REVIEW PROCESS</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/openaccesspolicy"><strong>OPEN ACCESS POLICY</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/copyrightnotice"><strong>COPYRIGHT NOTICE</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/license"><strong>LICENSE</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/about/editorialTeam"><strong>EDITORIAL TEAM</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/Reviewer"><strong>REVIEWER</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><a href="http://jurnal.sttsetia.ac.id/index.php/phr/about/contact"><strong>CONTACT</strong></a></p>
<p style="background-color: white; text-align: center; border: 1px solid white; border-radius: 5px;"><strong><a href="http://jurnal.sttsetia.ac.id/index.php/phr/indexing">INDEXING</a></strong></p>
</div>
</div>
</div>
</div>
</div>
<p style="padding: 10px; border-radius: text-align: center; border: 1px solid #aaa; background: #8508a5; color: black;"><strong>TEMPLATE</strong></p>
<p><a href="https://drive.google.com/file/d/1uknnkRe_Bt4DeJjqaXsQfXULIoLgeWvO/view"><img src="https://www.sttpb.ac.id/e-journal/public/site/images/evansiahaan/TFS1.png" alt="pasang"></a></p>',
    'id_ID' => '<p>&nbsp;</p>
<p>&nbsp;</p>',
  ),
  'context' => 1,
  'enabled' => true,
); ?>