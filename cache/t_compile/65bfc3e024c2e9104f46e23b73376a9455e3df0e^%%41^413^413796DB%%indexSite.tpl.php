<?php /* Smarty version 2.6.25-dev, created on 2021-02-12 09:21:13
         compiled from frontend/pages/indexSite.tpl */ ?>
 
 
 
 <!--- CUSTOM SAGARA ---->
 
 	
<meta name="description" content="Phronesis: Jurnal Teologi dan Misi">
<meta name="generator" content="Open Journal Systems 3.1.1.0">
<meta name="google-site-verification" content="V47YUtOA2zLqW_c-U-r_sLYHiWP2RiAOwAY9_KsZNWM" />
<link rel="alternate" type="application/atom+xml" href="http://jurnal.sttsetia.ac.id/index.php/phr/gateway/plugin/WebFeedGatewayPlugin/atom">
<link rel="alternate" type="application/rdf+xml" href="http://jurnal.sttsetia.ac.id/index.php/phr/gateway/plugin/WebFeedGatewayPlugin/rss">
<link rel="alternate" type="application/rss+xml" href="http://jurnal.sttsetia.ac.id/index.php/phr/gateway/plugin/WebFeedGatewayPlugin/rss2">
	<link rel="stylesheet" href="http://jurnal.sttsetia.ac.id/index.php/phr/$$$call$$$/page/page/css?name=stylesheet" type="text/css" /><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i" type="text/css" /><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" /><link rel="stylesheet" href="http://jurnal.sttsetia.ac.id/public/site/sitestyle.css" type="text/css" />
</head>	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g==" crossorigin="anonymous" />

 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="http://jurnal.sttsetia.ac.id/imgOJS/slider1.png" class="img-full" alt="Los Angeles">
      <div class="carousel-caption">
            <div class="row justify-content-center">
                <div class="col-md-3 col-6 col-sm-6 col-xs-6" > 
                    <a href="index.php/pkm" class="img-link">
                         <img src="http://jurnal.sttsetia.ac.id/imgOJS/LogoSD.png" alt="Los Angeles" width="100%">  
                    </a>
                </div> 
                <div class="col-md-3 col-6 col-sm-6 col-xs-6"> 
                    <a href="index.php/phr" class="img-link">
                        <img src="http://jurnal.sttsetia.ac.id/imgOJS/LogoPJ.png" alt="Los Angeles" width="100%">
                    </a>
                </div>
            </div>
      </div>   
    </div>
    <div class="carousel-item">
      <img src="http://jurnal.sttsetia.ac.id/imgOJS/slider2.png" class="img-full" alt="Chicago">
      <div class="carousel-caption">
        
      </div>   
    </div>
    <div class="carousel-item">
      <img src="http://jurnal.sttsetia.ac.id/imgOJS/slider3.png" class="img-full" alt="New York">
      <div class="carousel-caption">
         
      </div>   
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

<marquee style="font-family:Book Antiqua; color: #FFFFFF;padding:10px;font-size:20px!important;margin:0px!important;" bgcolor="#000080" >Hati orang berpengertian memperoleh pengetahunan, dan
telinga orang bijak menuntut pengetahuan (Amsal 18:15)</marquee>
 
 
 <!--- END CUSTOM --->
 