<?php return array (
  'plugins.importexport.doaj.displayName' => 'Mòdul d\'exportació DOAJ',
  'plugins.importexport.doaj.description' => 'Exporta la revista al DOAJ i envia informació sobre la revista al representant del DOAJ.',
  'plugins.importexport.doaj.export.contact' => 'Contacta amb el DOAJ per a l\'inclusió',
  'plugins.importexport.doaj.cliUsage' => 'Ús:
{$scriptName} {$pluginName} export [xmlFileName] [journal_path] articles objectId1 [objectId2] ...',
); ?>