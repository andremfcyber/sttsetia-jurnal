<?php return array (
  'plugins.generic.usageStats.optout.done' => '<p>Heu optat per sortir de la recopilació d\'estadístiques d\'ús amb èxit. Mentre vegeu aquest missatge no es recopilarà cap estadística de l\'ús d\'aquest lloc. Feu clic al botó de sota per revertir la decisió.</p>',
  'plugins.generic.usageStats.optin' => 'Incloure\'s',
  'plugins.generic.usageStats.optout' => 'Excloure\'s',
); ?>