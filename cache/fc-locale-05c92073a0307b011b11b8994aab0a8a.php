<?php return array (
  'plugins.gateways.resolver.displayName' => 'Plugin Resolver',
  'plugins.gateways.resolver.description' => 'Plugin ini memisahkan terbitan dan artikel berdasarkan informasi kutipan.',
  'plugins.gateways.resolver.errors.errorMessage' => 'Tidak bisa memisahkan kesatuan tunggal dengan menggunakan informasi kutipan. Pastikan informasi kutipan dapat dimengerti dan mengacu ke kesatuan tunggal dalam penempatan jurnal ini.',
  'plugins.gateways.resolver.exportHoldings' => 'Ekspor Saham',
); ?>