<?php return array (
  'plugins.importexport.medra.displayName' => 'Mòdul d\'exportació/registre mEDRA',
  'plugins.importexport.medra.description' => 'Export issue, article and galley metadata in Onix for DOI (O4DOI) format and register DOIs with the mEDRA registration agency.',
); ?>