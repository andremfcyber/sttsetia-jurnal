<?php return array (
  'plugins.importexport.datacite.displayName' => 'Mòdul d\'exportació/registre DataCite',
  'plugins.importexport.datacite.settings.form.username' => 'Nom d\'usuari (símbol)',
  'plugins.importexport.datacite.cliUsage' => 'Ús:
{$scriptName} {$pluginName} export [outputFileName] [journal_path] {issues|articles|galleys} objectId1 [objectId2] ...
{$scriptName} {$pluginName} register [journal_path] {issues|articles|galleys} objectId1 [objectId2] ...',
); ?>