<?php return array (
  'agenciesEnabledSubmission' => false,
  'agenciesEnabledWorkflow' => false,
  'agenciesRequired' => false,
  'citationsEnabledSubmission' => true,
  'citationsEnabledWorkflow' => true,
  'citationsRequired' => true,
  'contactEmail' => 'malikdariausbambangan@gmail.com',
  'contactName' => 'Malik',
  'contactPhone' => '',
  'copySubmissionAckAddress' => 'info@sttsetia.ac.id',
  'copySubmissionAckPrimaryContact' => true,
  'coverageEnabledSubmission' => false,
  'coverageEnabledWorkflow' => false,
  'coverageRequired' => false,
  'defaultReviewMode' => 2,
  'disableUserReg' => false,
  'disciplinesEnabledSubmission' => true,
  'disciplinesEnabledWorkflow' => true,
  'disciplinesRequired' => true,
  'emailSignature' => '<br/>
________________________________________________________________________<br/>
<a href="{$contextUrl}">{$contextName}</a>',
  'enableAnnouncements' => true,
  'enableAnnouncementsHomepage' => true,
  'itemsPerPage' => 25,
  'keywordsEnabledSubmission' => true,
  'keywordsEnabledWorkflow' => true,
  'keywordsRequired' => true,
  'languagesEnabledSubmission' => true,
  'languagesEnabledWorkflow' => true,
  'languagesRequired' => true,
  'mailingAddress' => 'Grha Yesyurun',
  'membershipFee' => 0,
  'navItems' => NULL,
  'numAnnouncementsHomepage' => 2,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'numPageLinks' => 10,
  'numWeeksPerResponse' => 0,
  'numWeeksPerReview' => 4,
  'onlineIssn' => '2723-6617',
  'printIssn' => '2621-2684',
  'publicationFee' => 0,
  'publisherInstitution' => 'Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta',
  'publishingMode' => 0,
  'purchaseArticleFee' => 0,
  'restrictArticleAccess' => false,
  'restrictReviewerFileAccess' => true,
  'restrictSiteAccess' => false,
  'reviewerAccessKeysEnabled' => false,
  'reviewerCompetingInterestsRequired' => true,
  'rightsEnabledSubmission' => false,
  'rightsEnabledWorkflow' => false,
  'rightsRequired' => false,
  'rtAbstract' => true,
  'rtAddComment' => true,
  'rtCaptureCite' => true,
  'rtDefineTerms' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'rtPrinterFriendly' => true,
  'rtSupplementaryFiles' => true,
  'rtViewMetadata' => true,
  'showEnsuringLink' => true,
  'sourceEnabledSubmission' => false,
  'sourceEnabledWorkflow' => false,
  'sourceRequired' => false,
  'subjectsEnabledSubmission' => true,
  'subjectsEnabledWorkflow' => true,
  'subjectsRequired' => true,
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
  ),
  'supportedLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
  ),
  'supportEmail' => 'iwancahyo@gmail.com',
  'supportName' => 'Iwan Rumahorbo',
  'supportPhone' => '',
  'themePluginPath' => 'default',
  'typeEnabledSubmission' => false,
  'typeEnabledWorkflow' => false,
  'typeRequired' => false,
  'abbreviation' => 
  array (
    'en_US' => 'phr',
  ),
  'about' => 
  array (
    'en_US' => '<p><strong>Fokus dan Ruang Lingkup</strong></p>
<p style="margin-bottom: 0in; line-height: 100%;">Jurnal ini memuat karya ilmiah dari para peneliti dalam bidang teologi yang memberikan impplikasi kepada misi atau seputar teologi misi. Ruang lingkup jurnal ini meliputi:</p>
<p style="margin-bottom: 0in; line-height: 100%;">1. Teologi Biblika<br>2. Misiologi (Studi interkultural)<br>3. Teologi Sistematika<br>4. Teologi Pastoral<br>5. Pendidikan Agama Kristen<br>6. Teologi Praktika</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Etika Publikasi</strong></p>
<p style="margin-bottom: 0in; line-height: 100%;">Jurnal Phronesis adalah jurnal yang dikelolah oleh Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta yang dipublikasikan oleh Departemen Publikasi dan Media Arastamar secara berkala dua kali setiap tahunnya pada bulan Januari dan Juli.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Jurnal Phronesis menggunakan sistem Blind-reviewer yang akan mempublikasikan hasil penelitian dari berbagai tempat yang memiliki kontribusi kepada bidang teologi dan misi di Indonesia. Untuk menjaga kualitas maka jurnal ini menyatakan etika publikasi seperti dibawah ini:</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;"><span style="text-decoration: underline;">Panduan Etika Publikasi Jurnal Phronesis (JP)</span></p>
<p style="margin-bottom: 0in; line-height: 100%;">Publikasi artikel Jurnal PHRONESIS adalah sebuah fondasi penting dalam pengembangan jaringan pengetahuan yang koheren dan dihormati. Ini merupakan cerminan langsung dari kualitas kerja para penulis dan lembaga-lembaga yang mendukung mereka. Artikel-artikel yang diulas sejawat mendukung dan mewujudkan metode ilmiah. Oleh karena itu penting untuk menyetujui standar perilaku etika yang diharapkan untuk semua pihak yang terlibat dalam tindakan penerbitan: penulis, editor, reviewer, penerbit, dan masyarakat. Sebagai penerbit Jurnal PHRONESIA, Dewan Editor mengambil tugas atas semua tahap penerbitan secara serius dan mengakui tanggung jawab etis dan lainnya. Sekolah Tinggi Theologia Injili Arastamar (SETIA) Jakarta berkomitmen untuk memastikan bahwa proses penerbitan jurnal mengikuti asas-asas akademik yang profesional.&nbsp;&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;">Keputusan Publikasi</p>
<p style="margin-bottom: 0in; line-height: 100%;">Editor Jurnal PHRONESIS bertanggung jawab untuk memutuskan artikel mana yang diserahkan ke jurnal yang harus diterbitkan. Validasi karya yang dipertanyakan dan kepentingannya bagi para peneliti dan pembaca harus selalu mendorong keputusan semacam itu. Para editor dapat dipandu oleh kebijakan dari dewan editor jurnal dan dibatasi oleh persyaratan hukum seperti yang berlaku saat ini terkait dengan pencemaran nama baik, pelanggaran hak cipta dan plagiarisme. Para editor dapat berunding dengan editor atau pengulas hal lain dalam membuat keputusan ini. Oleh karena itu adalah penting untuk setiap yang bersangkutan memperhatikan beberapa hal:</p>
<p style="margin-bottom: 0in; line-height: 100%;">Keadilan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Editor setiap saat mengevaluasi manuskrip untuk konten intelektual tanpa memperhatikan ras, jenis kelamin, asal etnis, kewarganegaraan, perbedaan institusi atau filsafat politik penulis.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Kerahasiaan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Editor dan staf editorial tidak boleh mengungkapkan informasi apa pun tentang naskah yang dikirimkan kepada siapa pun selain dari penulis, editorial lain, dan penerbit yang sesuai, sebagaimana mestinya.Team editorial harus menjaga kerahasiaan penulis selama proses evaluasi.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengungkapan dan konflik kepentingan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Setiap materi yang tidak dipublikasikan dan yang dinyatakan dalam manuskrip dan dikirimkan tidak boleh digunakan dalam penelitian editor sendiri tanpa persetujuan tertulis dari penulis.</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;"><span style="text-decoration: underline;">Tugas Reviewer</span></p>
<p style="margin-bottom: 0in; line-height: 100%;">Kontribusi kepada Editor</p>
<p style="margin-bottom: 0in; line-height: 100%;">Reviewer membantu editor dalam membuat keputusan editorial dan melalui komunikasi editorial dengan penulis juga dapat membantu penulis dalam meningkatkan makalah.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Kerahasiaan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Manuskrip yang diterima untuk ditinjau harus diperlakukan sebagai dokumen rahasia. Mereka tidak boleh ditunjukkan atau didiskusikan dengan orang lain kecuali sebagaimana diizinkan oleh editor.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Standar Objektivitas</p>
<p style="margin-bottom: 0in; line-height: 100%;">Tinjauan harus dilakukan secara obyektif. Kritik pribadi dari penulis tidak tepat. Reviewer harus mengungkapkan pandangan mereka dengan jelas dengan argumen pendukung.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengakuan Sumber</p>
<p style="margin-bottom: 0in; line-height: 100%;">Reviewer harus mengidentifikasi karya yang diterbitkan relevan yang belum dikutip oleh penulis. Setiap pernyataan bahwa observasi, derivasi, atau argumen telah dilaporkan sebelumnya harus disertai dengan kutipan yang relevan. Peninjau juga harus meminta perhatian editor jika ada kemiripan substansial atau tumpang tindih antara naskah yang sedang dipertimbangkan dan makalah lain yang diterbitkan yang memiliki pengetahuan pribadi.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengungkapan dan Konflik Kepentingan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Informasi atau ide istimewa yang diperoleh melalui peer review harus dijaga kerahasiaannya dan tidak digunakan untuk keuntungan pribadi. Reviewer tidak boleh mempertimbangkan manuskrip di mana mereka memiliki konflik kepentingan yang dihasilkan dari persaingan, kolaboratif, atau hubungan atau hubungan lain dengan salah satu penulis, perusahaan, atau lembaga yang terhubung dengan makalah.</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>
<p style="margin-bottom: 0in; line-height: 100%;"><span style="text-decoration: underline;">Tugas Peneliti</span></p>
<p style="margin-bottom: 0in; line-height: 100%;">Standar pelaporan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Penulis laporan penelitian asli harus menyajikan laporan akurat tentang pekerjaan yang dilakukan serta diskusi obyektif dari signifikansinya. Data yang mendasari harus diwakili secara akurat di koran. Sebuah makalah harus memuat detail dan referensi yang cukup untuk memungkinkan orang lain mereplikasi pekerjaan. Pernyataan yang curang atau dengan sengaja tidak akurat merupakan perilaku yang tidak etis dan tidak dapat diterima.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Orisinalitas dan Plagiarisme</p>
<p style="margin-bottom: 0in; line-height: 100%;">Para penulis harus memastikan bahwa mereka telah menulis karya yang sepenuhnya asli, dan jika penulis telah menggunakan karya dan / atau kata-kata orang lain yang telah dikutip atau dikutip dengan tepat.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Publikasi Ganda, Berlebihan, atau Bersamaan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Seorang penulis seharusnya tidak secara umum menerbitkan naskah yang menjelaskan pada dasarnya penelitian yang sama di lebih dari satu jurnal atau publikasi utama. Mengirimkan naskah yang sama ke lebih dari satu jurnal secara bersamaan merupakan perilaku penerbitan yang tidak etis dan tidak dapat diterima.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengakuan Sumber</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengakuan yang tepat atas karya orang lain harus selalu diberikan. Penulis harus mengutip publikasi yang telah berpengaruh dalam menentukan sifat pekerjaan yang dilaporkan.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengarang Tulis</p>
<p style="margin-bottom: 0in; line-height: 100%;">Karangan harus dibatasi bagi mereka yang telah memberikan kontribusi signifikan pada konsepsi, desain, pelaksanaan, atau interpretasi dari penelitian yang dilaporkan. Semua orang yang telah membuat kontribusi signifikan harus terdaftar sebagai rekan penulis. Di mana ada orang lain yang telah berpartisipasi dalam aspek substantif tertentu dari proyek penelitian, mereka harus diakui atau terdaftar sebagai kontributor. Penulis yang sesuai harus memastikan bahwa semua rekan penulis yang tepat dan tidak ada rekan penulis yang tidak pantas dimasukkan di atas kertas, dan bahwa semua rekan penulis telah melihat dan menyetujui versi akhir makalah ini dan telah menyetujui pengajuannya untuk publikasi.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Pengungkapan dan Konflik Kepentingan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Semua penulis harus mengungkapkan dalam naskah mereka setiap konflik keuangan atau konflik substantif lainnya yang mungkin ditafsirkan untuk mempengaruhi hasil atau interpretasi naskah mereka. Semua sumber dukungan keuangan untuk proyek harus diungkapkan.</p>
<p style="margin-bottom: 0in; line-height: 100%;">Kesalahan mendasar dalam karya yang diterbitkan</p>
<p style="margin-bottom: 0in; line-height: 100%;">Ketika seorang penulis menemukan kesalahan atau ketidaktepatan yang signifikan dalam karyanya yang diterbitkan, adalah kewajiban penulis untuk segera memberi tahu editor jurnal atau penerbit dan bekerja sama dengan editor untuk menarik kembali atau memperbaiki penelitiannya tersebut.</p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>',
  ),
  'acronym' => 
  array (
    'en_US' => 'phr',
  ),
  'announcementsIntroduction' => 
  array (
    'en_US' => '<p><span style="color: rgba(0, 0, 0, 0.87); font-family: \'Noto Serif\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen-Sans, Ubuntu, Cantarell, \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Jurnal Phronesis: Jurnal Teologi dan Misi menerima pemikiran-pemikiran baik dari dosen, mahasiswa, pendeta, aktivis gereja, guru, dan pihak-pihak lain dalam bentuk artikel untuk diterbitkan pada Vol. 3 No (2), November 2020. Artikel merupakan hasil-hasil penelitian pada ruang lingkup Studi Bilbika, Teologi Sistematika, Manajemen dan Hukum Gereja, Teologi Praktika,&nbsp; dan Misi. Artikel yang dikirim adalah artikel yang belum diterbitkan di jurnal lain. Artikel mengikuti template Jurnal Phronesis</span><span style="color: rgba(0, 0, 0, 0.87); font-family: \'Noto Serif\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen-Sans, Ubuntu, Cantarell, \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">&nbsp;dan disubmit di Jurnal Phronesis</span><span style="color: rgba(0, 0, 0, 0.87); font-family: \'Noto Serif\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen-Sans, Ubuntu, Cantarell, \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"> paling lambat 10 Oktober 2020. Bagi yang belum memiliki akun di jurnal Phronesis, maka perlu membuat akun terlebih dahulu di laman Jurnal Phronesis</span><span style="color: rgba(0, 0, 0, 0.87); font-family: \'Noto Serif\', -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen-Sans, Ubuntu, Cantarell, \'Helvetica Neue\', sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">. Untuk informasi lebih lanjut, penulis dapat menghubungi editor jurnal ini (Dr. Malik), No WA: +62821-8840-4070. Tuhan memberkati.</span></p>',
  ),
  'authorInformation' => 
  array (
    'en_US' => 'Interested in submitting to this journal? We recommend that you review the <a href="http://jurnal.sttsetia.ac.id/index.php/phr/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="http://jurnal.sttsetia.ac.id/index.php/phr/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="http://jurnal.sttsetia.ac.id/index.php/phr/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="http://jurnal.sttsetia.ac.id/index.php/index/login">log in</a> and begin the five-step process.',
  ),
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.',
  ),
  'clockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://clockss.org/">More...</a>',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta',
  ),
  'contactTitle' => 
  array (
    'en_US' => 'Dr.',
  ),
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections.

The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.


<h4>Copyediting Systems</h4>

<strong>1. Microsoft Word\'s Track Changes</strong>

Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author.

The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets.

After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage.


<strong>2. Harvard Educational Review</strong>

<strong>Instructions for Making Electronic Revisions to the Manuscript</strong>

Please follow the following protocol for making electronic revisions to your manuscript:

<strong>Responding to suggested changes.</strong>
&nbsp; For each of the suggested changes that you accept, unbold the text.
&nbsp; For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it.

<strong>Making additions and deletions.</strong>
&nbsp; Indicate additions by <strong>bolding</strong> the new text.
&nbsp; Replace deleted sections with: <strong>[deleted text]</strong>.
&nbsp; If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>.

<strong>Responding to Queries to the Author (QAs).</strong>
&nbsp; Keep all QAs intact and bolded within the text. Do not delete them.
&nbsp; To reply to a QA, add a comment after it. Comments should be delimited using:
<strong>[Comment:]</strong>
&nbsp; e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>.

<strong>Making comments.</strong>
&nbsp; Use comments to explain organizational changes or major revisions
&nbsp; e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>
&nbsp; Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.

<h4>An Illustration of an Electronic Revision</h4>

<ol>
<li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li>
<li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li>
<li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li>
</ol>',
  ),
  'customHeaders' => 
  array (
    'en_US' => '<meta name="google-site-verification" content="V47YUtOA2zLqW_c-U-r_sLYHiWP2RiAOwAY9_KsZNWM" />',
  ),
  'description' => 
  array (
    'en_US' => '<p>Phronesis adalah jurnal ilmiah dalam bidang teologi dan misi. Phronesis dikelolah bersama antara Perkumpulan Sekolah Arastamar dan Setia se Indonesia (PRESTASI) bersama Sekolah Tinggi Teologi Injili Arastamar Jakarta sejak 2018 secara cetak. Mulai tahun 2020 semester pertama memulai dengan sistem Open Journal System. Jurnal Phronesis: Teologi dan misi memuat artikel ilmiah di bidang teologi, biblika, Sistematika dan Misi yang mengedepankan kebaharuan ilmu di bidang Teologi. Indonesia merupakan konteks kekristenan yang mengalami perkembangan yang terbuka dengan gagasan-gagasan yang perlu dikontekstualisasikan namun demikian memelihara ajaran yang Alkitabiah.</p>',
  ),
  'editorialTeam' => 
  array (
    'en_US' => '<p><strong>Editor in Chief<br></strong><a href="https://scholar.google.co.id/citations?user=6Sr30oYAAAAJ&amp;hl=en">Malik Bambangan</a>, (SINTA ID: Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta</p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Jurnal Manager<br></strong></p>
<p style="margin-bottom: 0in; line-height: 100%;"><a href="https://scholar.google.com/citations?hl=id&amp;user=4-M30YkAAAAJ">Lewi Nataniel Bora</a>, Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta</p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Editor Section<br></strong></p>
<p style="margin-bottom: 0in; line-height: 100%;"><a href="https://scholar.google.co.id/citations?user=JtuKcowAAAAJ&amp;hl=en">Made Nopen Supriadi</a>, Sekolah Tinggi Teologi Arastamar Bengkulu<br><strong><br>Layout Editor&nbsp;</strong></p>
<p style="margin-bottom: 0in; line-height: 100%;"><a href="https://scholar.google.com/citations?user=fdQYLH4AAAAJ&amp;hl=en">Tony Salurante</a>, Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta<br><br></p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Copyeditor</strong><br><br>Okto Saul Tande Maure, Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta</p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Designer</strong></p>
<p style="margin-bottom: 0in; line-height: 100%;">Iwan Cahyo Rumahorbo , Sekolah Tinggi Teologi Injili Arastamar (SETIA) Jakarta</p>
<p style="margin-bottom: 0in; line-height: 100%;"><strong>Editor Guest</strong></p>
<p style="margin-bottom: 0in; line-height: 100%;"><a href="https://scholar.google.co.id/citations?user=DNobx9AAAAAJ&amp;hl=en">Adi Putra</a>, &nbsp;Sekolah Tinggi Teologi Pelita Dunia<br><a href="https://scholar.google.com/citations?user=2IbuWb0AAAAJ&amp;hl=en">Deky Hidnas Yan Nggadas </a>Sekolah Tinggi Teologi Huperetes, Batam<br><br><br><br></p>
<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>',
  ),
  'journalThumbnail' => 
  array (
    'en_US' => 
    array (
      'name' => 'Cover Phro.png',
      'uploadName' => 'journalThumbnail_en_US.png',
      'width' => 1284,
      'height' => 1723,
      'dateUploaded' => '2020-07-10 07:53:19',
      'altText' => '',
    ),
  ),
  'librarianInformation' => 
  array (
    'en_US' => 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://www.lockss.org/">More...</a>',
  ),
  'metaCitations' => 
  array (
    'en_US' => '1',
  ),
  'name' => 
  array (
    'en_US' => 'Phronesis: Jurnal Teologi dan Misi',
  ),
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.',
  ),
  'pageFooter' => 
  array (
    'en_US' => '<p>Address<br>Jl. Daan Mogot KM 18, Kebon Besar, Batuceper, Kota Tangerang, Banten, 151`22</p>
<p>email<br>jurnalphronesis@sttsetia.ac.id</p>
<p>Phone<br>+62821-8840-4070<br>+62822-5886-1613</p>
<!-- Default Statcounter code for Phronesis: Jurnal Teologi
dan Mi http://jurnal.sttsetia.ac.id/index.php/phr/ --><noscript><div class="statcounter"><a title="Web Analytics"
href="https://statcounter.com/" target="_blank"><img
class="statcounter"
src="https://c.statcounter.com/12388875/0/f8a442f9/0/"
alt="Web Analytics"></a></div></noscript><!-- End of Statcounter Code -->
<p><a href="https://statcounter.com/p12388875/?guest=1">View My Stats</a></p>',
  ),
  'pageHeaderLogoImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'Header Phronesis Baru.png',
      'uploadName' => 'pageHeaderLogoImage_en_US.png',
      'width' => 3750,
      'height' => 781,
      'dateUploaded' => '2020-08-28 09:52:44',
      'altText' => '',
    ),
  ),
  'privacyStatement' => 
  array (
    'en_US' => '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>',
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p>
	<h4>For Spelling and Grammar Errors</h4>

	<p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p>

	<pre>1. CHANGE...
	then the others
	TO...
	than the others</pre>
	<br />
	<pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre>
	<br />

	<h4>For Formatting Errors</h4>

	<p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p>
	<br />
	<pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre>
	<br />
	<pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
  ),
  'readerInformation' => 
  array (
    'en_US' => 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="http://jurnal.sttsetia.ac.id/index.php/phr/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="http://jurnal.sttsetia.ac.id/index.php/phr/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4>
	<p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p>
	<h4>A. When the Author Provides a Link with the Reference</h4>
	<ol>
	<li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>B. Enabling Readers to Search Google Scholar For References</h4>
	<ol>
		<li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., "Peace"—then copy author and title).</li>
		<li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&hl=en&lr=&btnG=Search.</li>

	<li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>C. Enabling Readers to Search for References with a DOI</h4>
	<ol>
	<li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li>
	<li>Paste each DOI that the Query provides in the following URL (between = and &): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&link_type=DOI.</li>
	<li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li>
	</ol>',
  ),
  'searchDescription' => 
  array (
    'en_US' => 'Phronesis: Jurnal Teologi dan Misi',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'content' => 'The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).',
        'order' => '1',
      ),
      1 => 
      array (
        'content' => 'The submission file is in OpenOffice, Microsoft Word, or RTF document file format.',
        'order' => '2',
      ),
      2 => 
      array (
        'content' => 'Where available, URLs for the references have been provided.',
        'order' => 6,
      ),
      3 => 
      array (
        'content' => 'The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.',
        'order' => '4',
      ),
      4 => 
      array (
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.',
        'order' => '5',
      ),
    ),
  ),
); ?>